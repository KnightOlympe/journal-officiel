const fs = require("node:fs");
const XBBCODE = require("./parser/xbbcode.js");

/**
 * 7 arguments are supposed to be passed :
 * 0) "node"
 * 1) the name of this file
 * 2) the path to the file containing the BBCode
 * 3) the CSS file to link
 * 4) the link to the file's BBCode
 * 5) the link to the file's history
 * 6) the link to the file's blame
 * 7) the link to the index
 */
if (process.argv.length !== 8) return;
const filePath = process.argv.at(2);
const CSSFile = process.argv.at(3);
const BBCodeLink = process.argv.at(4);
const fileHistory = process.argv.at(5);
const fileBlame = process.argv.at(6);
const indexPath = process.argv.at(7);

fs.readFile(filePath, "utf8", (err, toParse) => {
    if (err) {
        console.error(err);
        return;
    }

    const parsed = XBBCODE.process({
        text: toParse,
        removeMisalignedTags: true,
        addInLineBreaks: true
    });

    parsed.html = '<link rel="stylesheet" href="' + CSSFile + '">' //Adds a link to the CSS file
                + '<div class="links-block">'
                + '<p class="link">Retour à l\'<a href="' + indexPath + '">index du journal officiel</a></p>'  //Adds a link to the BBCode file
                + '<p class="link">Voir le <a href="' + BBCodeLink + '">BBCode</a></p>'  //Adds a link to the BBCode file
                + '<p class="link">Voir l\'<a href="' + fileHistory + '">historique</a></p>'  //Adds a link to the history of the file
                + '<p class="link">Voir la <a href="' + fileBlame + '">dernière modification par ligne</a> (git blame)</p>'  //Adds a link to the blame
                + '</div><br>'
                + parsed.html;

    fs.writeFile("parsed.html", parsed.html, err => {
        if (err) console.error(err);
    });
})
