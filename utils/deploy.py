#!/usr/bin/env python3

import os
from subprocess import call

def main():
    #Reads the config file
    with open("config") as config:
        for l in config.readlines():
            if l.startswith("gitlab_path="):
                gitlab_path = l[12:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("index_path="):
                index_path = l[11:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("default_branch="):
                default_branch = l[15:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("forum_link="):
                forum_link = l[11:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("image_main_page="):
                image_main_page = l[16:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("title_tab="):
                title_tab = l[10:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("title_main_page="):
                title_main_page = l[16:].replace("'", "\'").replace("\"", "\\")
            elif l.startswith("favicon="):
                favicon = l[8:].replace("'", "\'").replace("\"", "\\")

    #Creates public directory and subdirectories
    if not(os.path.isdir("../public")):
        os.mkdir("../public")
    if not(os.path.isdir("../public/Textes")):
        os.mkdir("../public/Textes")
    for path, subdirs, files in os.walk("../Textes"):
        file_path = "../public" + path[2:]
        if not(os.path.isdir(file_path)):
            os.mkdir(file_path)

    #Adds CSS file
    call("cp ./parser/xbbcode.css ../public/xbbcode.css", shell=True)
    css_file_name = "xbbcode.css"

    #Parses files
    for path, subdirs, files in os.walk("../Textes"):
        for file in files:
            if file[-7:] != ".bbcode":
                continue
            file_path = path + "/" + file
            css_file_path = css_file_name
            for i in range(file_path.count("/") - 1):
                css_file_path = "../" + css_file_path
            public_file_path = "../public" + path[2:] + "/" + file[:-7] + ".html"
            remote_bbcode_path = gitlab_path + "-/blob/" + default_branch + file_path[2:] + "?ref_type=heads"
            remote_history_path = gitlab_path + "-/commits/" + default_branch + file_path[2:] + "?ref_type=heads"
            remote_blame_path = gitlab_path + "-/blame/" + default_branch + file_path[2:] + "?ref_type=heads"
            call("node parser_launcher.js \"" + file_path + "\" \"" + css_file_path + "\" \"" + remote_bbcode_path + "\" \"" + remote_history_path + "\" \"" + remote_blame_path + "\" \"" + index_path + "\"", shell=True)
            call("mv parsed.html \"" + public_file_path + "\"", shell=True)

    #Creates index
    index = '<!DOCTYPE html><html><head><link rel="stylesheet" href="xbbcode.css">' \
            + '<title>' + title_tab + '</title>' \
            + '<link rel="icon" type="image/x-icon" href="' + favicon + '"></head>' \
            + '<body><div style="text-align:center">' \
            + '<div class="links-block">' \
            + '<p class="link"><a href="' + forum_link + '">Retourner au forum</a></p>' \
            + '<p class="link"><a href="' + gitlab_path + '">Voir les sources</a></p>' \
            + '</div><br>' \
            + '<img alt="Drapeau" src="' + image_main_page + '" decoding="async" width="280" height="170" /><div><span style="color:#000000;;font-size:50px">' + title_main_page + '<big>&#160;</big></span></div>' \
            + '</div>' \
            + '<h2>Liste des textes disponibles : </h2><div><ul>'
    listSubdirsEnd = []
    for path, subdirs, files in os.walk("../Textes"):
        if path == "../Textes":
            continue
        index += "<li>" + os.path.basename(path) + "<ul>"
        if len(subdirs) != 0:
            listSubdirsEnd.append(path + "/" + subdirs[-1])
        for f in files:
            if f[-7:] != ".bbcode":
                continue
            index += "<li><a href = \"" + path + "/" + f[:-7] + ".html\">" + f[:-7] + "</a></li>"
        if len(subdirs) == 0:
            index += "</ul>"
        if path in listSubdirsEnd:
            listSubdirsEnd.remove(path)
            index += "</ul>"
    index += "</ul></div>" \
            + "</body></html>"
    with open("../public/index.html", "w") as f:
        f.write(index)

main()