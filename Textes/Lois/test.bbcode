[quote][center][b][size=170]Loi relatif à l’instauration d’un statut particulier pour la région de Cavour[/size][/b][/center]



[i]Depuis plusieurs années, la République d’Ostaria connaît de fortes disparités socio-économiques entre sa métropole et la région de Cavour. C’est en considérant ces disparités et avec la conviction de renforcer l’unité républicaine que l’Assemblée Nationale reconnaît la nécessité impérative d’accorder un statut particulier à la région de Cavour. Ce statut a pour objectif de lui conférer davantage de responsabilités, tout en préservant le contrôle de l’État et l’indivisibilité de la République.[/i]


[size=150][b][u]Titre Ier - Dispositions générales[/u][/b][/size]

[b]Article 1.-[/b]
Tous les articles du Titre troisième du Code des Collectivités territoriales sont abrogés.

[b]Article 2.-[/b]
Il est ajouté, dans le Titre troisième du Code des Collectivités territoriales, l’article 300-1 rédigé comme suit :
[quote]La République d’Ostaria se divise en six régions : cinq sont des régions métropolitaines et une dispose d’un statut particulier. Elles conservent les délimitations territoriales actuelles.
Les régions métropolitaines sont du nom de Aupagne, Bacapy, Brifalle, Choignaux et Orbône.
La région à statut particulier est du nom de Cavour.[/quote]


[size=150][b][u]Titre II - Des régions[/u][/b][/size]

[b]Article 3.-[/b]
Il est ajouté, dans le Titre troisième du Code des Collectivités territoriales, un Chapitre premier intitulé “Des régions métropolitaines”.
Le Chapitre premier du Titre troisième du Code des Collectivités territoriales est rédigé comme suit :
[quote][size=130][b]Chapitre Ier - Des régions métropolitaines[/b][/size]

[b]Article 301-1.-[/b]
Toute région métropolitaine de la République d’Ostaria est dotée d’un Conseil Régional, élu selon les conditions prévues par la loi.
Le Conseil Régional est composé d’autant de membres que la racine cubique de la population de la région.

[b]Article 301-2.-[/b]
Toute région métropolitaine de la République d’Ostaria est dotée d’un Président du Conseil Régional élu par le Conseil Régional.
Après démission, révocation, décès du Président du Conseil Régional ou renouvellement du Conseil Régional, il doit être procédé à l’élection d’un nouveau Président du Conseil Régional. Tout membre du Conseil Régional est autorisé à se porter candidat dans le cadre d’une séance exceptionnelle du Conseil présidée par le doyen du Conseil Régional.
Chaque conseiller régional dispose d’une voix et il est organisé autant de tours de scrutin que nécessaire pour qu’un candidat obtienne la majorité absolue des suffrages exprimés.

[b]Article 301-3.-[/b]
Par un vote au trois cinquièmes du Conseil Régional, le Président du Conseil Régional peut être révoqué par le Conseil Régional.

[b]Article 301-4.-[/b]
La Région métropolitaine est compétente concernant :
    - Les transports intercommunaux;
    - La gestion et l’administration de la Région;
    - Les infrastructures de la Région hors des agglomérations ou prises en charge par la Région;
    - Les politiques environnementales touchant à la Région;
    - Les grands travaux intercommunaux;
    - Les infrastructures d’enseignement secondaire et supérieur;
    - L’administration des services publics;
    - Les politiques culturelles et sportives.

[b]Article 301-5.-[/b]
Le Président du Conseil Régional est autorisé à signer des Arrêtés régionaux pour agir sur l’une des thématiques de l’article 301-4 sans l’aval du Conseil Régional tant que l’arrêté ne prévoit pas de modifications des dépenses et des recettes communales prévues par le budget communal, comme prévu dans l’article 301-6 de la présente loi.

[b]Article 301-6.-[/b]
Le Conseil Régional doit chaque année voter à la majorité simple un budget pour établir les recettes et les dépenses régionales pour l’année suivante.[/quote]

[b]Article 4.-[/b]
Il est ajouté, dans le Titre troisième du Code des Collectivités territoriales, un Chapitre deuxième intitulé “Du statut particulier de la région de Cavour”.
Il est ajouté, dans le Chapitre deuxième du Titre troisième du Code des Collectivités territoriales, l’article 302 rédigé comme suit :
[quote]La région de Cavour est dotée d’un statut particulier lui accordant une autonomie administrative renforcée et l’exercice de certaines compétences propres, dans le respect de l’unité nationale, et en étroite collaboration avec les institutions de l’État.[/quote]

[b]Article 5.-[/b]
Il est ajouté, dans le Chapitre deuxième du Titre troisième du Code des Collectivités territoriales, une section 1 intitulée “De la gouvernance régionale de Cavour”.
La section 1 du Chapitre deuxième du Titre troisième du Code des Collectivités territoriales est rédigé comme suit :
[quote][size=110][b]Section 1 - De la gouvernance régionale de Cavour[/b][/size]

[b]Article 302-1-1.-[/b]
La région de Cavour est dotée d’une Assemblée régionale, élue selon les conditions prévues par la loi pour les élections régionales. Elle est composée d’un nombre de membres correspondant à la racine cubique de la population de la région.
L’Assemblée régionale est l’organe législatif de la région, responsable de l’adoption des arrêtés régionaux et du budget.

[b]Article 302-1-2.-[/b]
L’Assemblée régionale est présidée par un Président, élu par ses membres à la majorité absolue au cours de sa première session. Il est chargé de la présidence et de la supervision des débats.

[b]Article 302-1-3.-[/b]
Le Conseil de Cavour constitue l’organe exécutif de la région. Il est composé d’un président et de six conseillers cavourois en charge des domaines suivants :
    - Les infrastructures, les mobilités et les transports ;
    - La jeunesse, l’éducation, la formation et l’emploi ;
    - Le développement économique, les finances, les entreprises et l’innovation ;
    - L’énergie, l’environnement, l’alimentation, la mer, le développement agricole et rural ;
    - La culture, le patrimoine, les sports, la vie associative et l’égalité homme-femme ;
    - L’aménagement du territoire, l’urbanisme et le tourisme.

[b]Article 302-1-4.-[/b]
Le Président du Conseil de Cavour est nommé par l’Assemblée régionale à la majorité absolue parmi les mêmes élus de l’Assemblée, après présentation de son programme d’action.
Le Président du Conseil de Cavour détient le pouvoir exécutif dans la région, il nomme et révoque les conseillers cavourois, sur approbation du Gouvernement, pour diriger les domaines énumérés à l’article 302-1-3.
Le Conseil de Cavour demeure soumis au contrôle de légalité du ministère chargé des affaires cavouroises.

[b]Article 302-1-5.-[/b]
Le Conseil de Cavour, sous l’autorité de son président, est chargé de mettre en œuvre les décisions de l’Assemblée régionale et d’administrer la région dans le cadre des compétences qui lui sont déléguées. Chaque conseiller gère son domaine de compétence et rend compte régulièrement de ses actions à l’Assemblée régionale.[/quote]

[b]Article 6.-[/b]
Il est ajouté, dans le Chapitre deuxième du Titre troisième du Code des Collectivités territoriales, une section 2 intitulée “Des mécanismes régionaux de contrôle en Cavour”.
La section 2 du Chapitre deuxième du Titre troisième du Code des Collectivités territoriales est rédigé comme suit :
[quote][size=110][b]Section 2 - Des mécanismes régionaux de contrôle en Cavour[/b][/size]

[b]Article 302-2-1.-[/b]
L’Assemblée régionale peut initier, à la demande d’un tiers des conseillers régionaux, une motion régionale de remplacement partiel, visant à remplacer un à trois conseillers du Conseil de Cavour.
La motion est adoptée si elle recueille l’avis favorable de la majorité absolue des membres de l’Assemblée régionale.
Si la motion est adoptée, les conseillers visés sont révoqués et le Président du Conseil de Cavour doit proposer de nouveaux conseillers pour les postes vacants, soumis à l’approbation du Gouvernement. 
La motion de remplacement partiel ne remet pas en cause la légitimité du Président du Conseil de Cavour.

[b]Article 302-2-2.-[/b]
L’Assemblée régionale peut également initier, à la demande des deux cinquièmes des conseillers régionaux, une motion régionale de remplacement intégral, dite “motion de contre-Conseil”, visant à proposer la mise en place d’un nouveau Conseil avec un nouveau président et de nouveaux conseillers.
La motion est adoptée si elle recueille l’avis favorable de la majorité absolue des membres de l’Assemblée régionale.
Si la motion est adoptée, le Conseil de Cavour est dissous et le Président proposé dans le contre-Conseil est investi des pouvoirs exécutifs. Sous réserve de l’approbation du Gouvernement, les conseillers cavourois proposés prennent leurs fonctions.[/quote]

[b]Article 7.-[/b]
Il est ajouté, dans le Chapitre deuxième du Titre troisième du Code des Collectivités territoriales, une section 3 intitulée “Des compétences spécifiques de la région de Cavour”.
La section 3 du Chapitre deuxième du Titre troisième du Code des Collectivités territoriales est rédigé comme suit :
[quote][size=110][b]Section 3 - Des compétences spécifiques de la région de Cavour[/b][/size]

[b]Article 302-3-1.-[/b]
En plus des compétences prévus à l’article 301-4 du présent code pour les régions métropolitaines, la région de Cavour se voit confier, par délégation de l’État, des compétences suivantes :
    - La protection et la valorisation du patrimoine culturel, linguistique et environnemental ;
    - La gestion et l’organisation des transports internes à l’île, y compris des infrastructures ;
    - La gestion des zones littorales et la régulation de l’activité économique liée au tourisme ;
    - La promotion de la langue et des traditions locales dans les programmes éducatifs, sans préjudice aux principes de laïcité et de neutralité républicaine ;
    - La participation à la gestion des ressources naturelles de l’île, en particulier celles relatives aux énergies renouvelables ;
    - La coordination des services de santé et des infrastructures sanitaires.

[b]Article 302-3-2.-[/b]
Dans l’exercice de ses compétences spécifiques, l’Assemblée régionale de Cavour peut adopter des arrêtés régionaux ayant force législative, dans les limites de leurs compétences.
Ces arrêtés doivent être conformes à la Constitution, aux lois nationales, aux directives gouvernementales et aux engagements internationaux de la République d’Ostaria.

[b]Article 302-3-3.-[/b]
La région de Cavour bénéficie d’une autonomie fiscale partielle. L’Assemblée régionale peut créer et moduler certaines taxes locales, sous réserve des dispositions législatives nationales.
L’État continue d’accorder un budget à la région, au même titre que les régions métropolitaines.[/quote]

[b]Article 8.-[/b]
Il est ajouté, dans le Chapitre deuxième du Titre troisième du Code des Collectivités territoriales, une section 4 intitulée “Du contrôle de l’État et des relations avec le Gouvernement”.
La section 4 du Chapitre deuxième du Titre troisième du Code des Collectivités territoriales est rédigé comme suit :
[quote][size=110][b]Section 4 - Du contrôle de l’État et des relations avec le Gouvernement[/b][/size]

[b]Article 302-4-1.-[/b]
L’administration ministérielle chargée des affaires cavouroises est responsable du contrôle de légalité des arrêtés pris par la région de Cavour, défini à l’article 302-3-2 du présent code.
En cas de non-conformité d’un arrêté régional aux lois ou aux directives nationales, le Premier Ministre peut, dans un cadre législatif, abroger ou modifier l’arrêté en question. De même, dans un cadre constitutionnelle, le Président de la République ou la Haute Cour Constitutionnelle disposent du pouvoir de déclarer l’inapplicabilité de tout arrêté régional contraire à la Constitution ou aux principes fondamentaux de la République d’Ostaria.

[b]Article 302-4-2.-[/b]
Aucune décision prise par les autorités régionales de Cavour ne doit porter atteinte à l’unité ou à l’intégrité de la République. Toute décision ou arrêté régional susceptible de compromettre ces principes fondamentaux peut faire l’objet d’un recours en annulation devant la Haute Cour Constitutionnelle.

[b]Article 302-4-3.-[/b]
L’État, en collaboration avec la région de Cavour, s’engage à mettre en œuvre des politiques spécifiques pour compenser les contraintes économiques, sociales et environnementales liées à l’insularité de la région.[/quote]

[b]Article 9.-[/b]
[i]Supprimé[/i]

[b]Article 10.-[/b]
Les dispositions de la présente loi entreront en vigueur au 1er janvier 232, après une période transitoire d’un an, durant laquelle le gouvernement et la région de Cavour prépareront la mise en application des dispositions de ce statut particulier.[/quote]




[right]Promulgué le [b]1er novembre 230[/b] à Lunont

[b]Christophe Letordu[/b],
[i]Président de la République d’Ostaria[/i].[/right][/quote]