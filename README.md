# Journal officiel de la République d'Ostaria

[![Journal officiel](./utils/badges/badge_jo.svg)](https://journal-officiel-ostaria-1873715556372689adad84c89d2983f0680670.gitlab.io/)
[![Discord](./utils/badges/badge_discord.svg)](https://discord.gg/qwmRZUU)
[![Ostaria](./utils/badges/badge_ostaria.svg)](https://ostaria.nations.fr)
[![Nations.fr](./utils/badges/badge_nations.svg)](https://nations.fr)

Ceci est le journal officiel de la [République d'Ostaria](https://ostaria.nations.fr/), une nation fictive évoluant dans l'univers de [Nations.fr](https://nations.fr/).
